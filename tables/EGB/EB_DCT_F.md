# EB_DCT_F

Table des données de décompte


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|DCT_MAN_DTD|date|Date de mandatement Initial|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|DCT_ARC_DTE|date|Date liquidation|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|DCT_ENT_SUP|nombre réel|Type de saisie affinée|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|DCT_RGU_SNS|chaîne de caractères|Sens de la régulation|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|DCT_MVT_SYS|nombre réel|Type de saisie|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|DCT_REM_DTD|date|Date de remboursement - recyclage|||
|DCT_ARR_DTE|date|Date arrivée du dossier|||
|DCT_CTL_COD|nombre réel|Code du contrôle|||
|DCT_MUT_CMP|chaîne de caractères|Part Mutuelle Complementaire décompte|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|CLE_TEC_DCT|chaîne de caractères|Clé technique Décompte|||
|DCT_CTL_TYP|chaîne de caractères|Type de Controle|||
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|DCT_LOT_TCR|chaîne de caractères|Type Certification du Lot|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|DCT_INT_VER|chaîne de caractères|Type de Version interface Tiers|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
