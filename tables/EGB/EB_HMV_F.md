# EB_HMV_F

table des mouvements des bénéficiaires de l'EGB


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|BEN_QLT|chaîne de caractères|Qualité du bénéficiaire (A=assuré AD=ayant-droit)|||
|ORG_AFF_DTF|date|Date de fin de rattachement à l'ancien organisme|||
|BEN_MVT_TOP|chaîne de caractères|Code des mouvements du bénéficiaire|||
|SLM_RTT_COD|chaîne de caractères|ancienne SLM de rattachement au RNIAM|||
|GRG_RTT_COD|chaîne de caractères|Grand régime de rattachement au RNIAM du bénéficiaire|||
|BEN_RES_DPT|chaîne de caractères|Département de résidence du bénéficiaire|||
|BEN_NIR_IDT|chaîne de caractères|NIR anonyme du bénéficiaire dans l'échantillon|||
|MAJ_SYS_DTE|date|Date de mise à jour des données du bénéficiaire dans le référentiel|||
|CAI_RTT_COD|chaîne de caractères|caisse de rattachement au RNIAM|||
